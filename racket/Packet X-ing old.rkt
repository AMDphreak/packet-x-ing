#lang racket

(require "Packet X-ing GUI.rkt")

;-- Fake inputs: --
(define (fake-data) (read-line (open-input-file "fake capture data.txt"))) ; dummy data

;; callbacks
(set-field! callback start_btn (lambda (btn evt) (main-section)))
(set-field! callback basic_filter (lambda (btn evt) (search)))
(set-field! callback advanced_filter (lambda (btn evt) (search)))

; utility functions

; Clip a string to 200 characters
(define (clip-string str)
  (if (> (string-length str) 200) (substring str 0 200) str))

; Set a list-box's field's string value
(define (setstring obj row col str)
  (send component set-string row str col))

; Searching:
; Searching is done with a combination of basic substring search
; and advanced syntax filters.
; input -> basicfilter -> advancedfilter -> results
(define (search)
  (send outputs add-child search_results) ; Make search results visible with add-child
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  
  )
(define (goto-packet n)
  (send packets set-first-visible-item n)
  )
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; main part of program
(define (main-section)
  (send welcome show #false) ; Hide Welcome stuff
  (send start_btn reparent controls) ; reparent Start Button
  

  
  (send rawdata_frame show #true)
  (send raw_output set )
  (send output set (list) (list))

;-- Outputs: --

(define (add-packet n)
  (setstring 
  (send packets set-data n (packet-data)))
(define (capture)
  
  (add-packet n))

; !---- Show the application window ----!
(send frame show #true)