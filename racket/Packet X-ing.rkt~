#lang racket/gui
; Packet Crossing (Xing)
;Packet sniffer and attack program

;Authors: Ryan Johnson, Mohammed Adem

;Requires a library that calls the system's operations to
;report all activity that goes through the network interface
;we will be using the sockets library and creating a raw socket

;;can use (require racket/gui/base) instead of #lang racket/gui
;(require racket/gui/base)

;-----------------------------------------------------------;;;;;;;;;;;;;;;;;;;;;;;
;Main Window (frame)
;
(define main_window
  (new frame%
       [label "Packet X-ing"]
       [width 800]
       [height 450]
       [min-width 600]
       [min-height 400]))

; |--------------|
; |              |
; |     frame    |
; |              |
; |--------------|



;;;;;;;;;;;;;;;;;;;;;;;;
;Panels
; 

;Base panel
(define base_panel (new horizontal-panel% [parent main_window]))
;base panel contains all other panels
;   ---------------------
;   |        |          |
;   |        |          |
;   |  hori  |  zontal  |
;   |        |          |
;   |        |          |
;   ---------------------
;base_panel is defined as a horizontal panel in case we want
;to stick side-panels in later:
; ----------------------
; |      |      |      |
; | side | main | side |
; |      |      |      |
; ----------------------

;Main panel
(define main_panel (new vertical-panel% [parent base_panel]))
; |--------------|
; |              |
; |     vert     |
; |              |
; |--------------|
; |              |
; |     ical     |
; |              |
; |--------------|

(define capt_button_panel (new vertical-panel% [parent main_panel]))
(define filters_panel (new vertical-panel% [parent main_panel]))
 (define filt_panel_lbl (new vertical-panel% [parent filters_panel]))
 (define filters_panelH (new horizontal-panel% [parent filters_panel]))
  (define search_box_panel (new vertical-panel% [parent filters_panelH]))
  (define advanced_filt_panel (new vertical-panel% [parent filters_panelH]))
  (define syntax_btn_panel (new vertical-panel% [parent filters_panelH]))
(define packets_output_panel (new vertical-panel% [parent main_panel]))
 (define search_results_panel (new vertical-panel% [parent packets_output_panel]))


;;;
;Gui Elements

;capture button
(new button% [parent capt_button_panel]
     [label "Capture"]
     [callback (lambda (button event) (begin-capture))])

;labels
(new message% [label "Filters"] [parent filt_panel_lbl])

;search box
(define search_box (new text-field% [parent search_box_panel]
                        [label "Search"]
                        [callback (lambda (t e) search)]))

;advanced filter
(define advanced_filter (new text-field% [parent advanced_filt_panel]
                             [label "Advanced filter"]))

;search results
(define search_results (new list-box% [parent search_results_panel]
                            [label "Search Results"]
                            [choices (list "")]
                            [columns '("Line" "Contents")]
                            [style (list
                                    'single
                                    'vertical-label
                                    'deleted
                                    'clickable-headers)]
                            [font (make-object font% 12.0 'default)]))

;Syntax help window
(define syntax_help_window (new frame%
                                [label "Syntax Help for Advanced Filter"]
                                [parent main_window]
                                [min-height 400]
                                [min-width 400]))

(define syntax_button (new button% [parent syntax_btn_panel]
       [label "Syntax rules"]
       [callback (lambda (button event) (syntax_help))]
       ;lambda discards the button and event then calls syntax_help
       ))

; Packet list
(define packets_output (new list-box% [parent packets_output_panel]
                    [label "Capture output"]
                    [choices (list "")]
                    [columns '("Line" "Contents")]
                    [style (list
                            'single
                            'vertical-label
                            'deleted)]
                    [font (make-object font% 12.0 'default)]))

; !---- Show the application window ----!
(send main_window show #true)

;;;;;;;;;;;;;;;;;;;;;
; Functions

  ;TODO: load the dimensions from the last window session

; Basic substring search:
(define (search)

  ; make search result list visible
  (send search_results_panel add-child search_results)
  (send search_results set ("1") ("you clicked search"))

  ;compare input string against current line
  ;(read-line fake_capture)
  ;how to get the text field in search box:
  ;(send search_box get-text)
  ;lowercase compare
  ;(string-downcase search_term)
  ;(string-downcase capture_string)

  )

; Advanced filter:

  ; fill in later

; Syntax help:
(define (syntax_help)
  ;Show syntax help window
  (send syntax_help_window show #true))

;-- Outputs: --

;;;;;;;;;;;
; Capture:

; dummy data
(define (fake-capture-data) (read-line (open-input-file "fake capture data.txt")))

; capture wrapper. Splits up the input string if greater than 200 chars long.
(define (begin-capture [capture-string (fake-capture-data)])
  ; default value is fake capture data:
  ; [] denotes a variable and its default value: [variable default_value]
  ; fake-capture function is invoked as default_value: (fake-capture)

  ;make output list-box visible:
  (send packets_output_panel add-child packets_output)
  
  (define (fill-listbox str count)        ; append the first 200 chars to the output
    (if (<= (string-length str) 200)      ; if string is <= 200 chars...
        (send packets_output set)         ; then fill the listbox
        ;General form: (send a-list-box set choices0 choices ...) | choices0 is a listof label-strings.
        (fill-listbox (substring str 199) (- count 1))))
  
  (define count (/ (string-length capture-string) 200))
  (fill-listbox capture-string count)) ; run the recursive alg on capture-string